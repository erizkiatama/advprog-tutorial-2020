package id.ac.ui.cs.advprog.tutorial1.strategy;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class StrategyInitializer {

    @Autowired
    private AdventurerRepository adventurerRepository;

    @Autowired
    private StrategyRepository strategyRepository;

    @PostConstruct
    public void init() {
//        Creating a constructor for each adventurer, not changing this init method
//        p.s. this one should be the answer, check each Adventurer class too see the constructor
        this.adventurerRepository.save(new AgileAdventurer());
        this.adventurerRepository.save(new KnightAdventurer());
        this.adventurerRepository.save(new MysticAdventurer());
        this.strategyRepository.addAttackBehavior(new AttackWithGun());
        this.strategyRepository.addAttackBehavior(new AttackWithMagic());
        this.strategyRepository.addAttackBehavior(new AttackWithSword());
        this.strategyRepository.addDefenseBehavior(new DefendWithBarrier());
        this.strategyRepository.addDefenseBehavior(new DefendWithArmor());
        this.strategyRepository.addDefenseBehavior(new DefendWithShield());


//        Not creating a constructor for each adventurer, changing this init method
//
//        AgileAdventurer agileAdventurer = new AgileAdventurer();
//        KnightAdventurer knightAdventurer = new KnightAdventurer();
//        MysticAdventurer mysticAdventurer = new MysticAdventurer();
//
//        AttackWithGun attackWithGun = new AttackWithGun();
//        AttackWithSword attackWithSword = new AttackWithSword();
//        AttackWithMagic attackWithMagic = new AttackWithMagic();
//
//        DefendWithArmor defendWithArmor = new DefendWithArmor();
//        DefendWithBarrier defendWithBarrier = new DefendWithBarrier();
//        DefendWithShield defendWithShield = new DefendWithShield();
//
//        agileAdventurer.setAttackBehavior(attackWithGun);
//        agileAdventurer.setDefenseBehavior(defendWithBarrier);
//
//        knightAdventurer.setAttackBehavior(attackWithSword);
//        knightAdventurer.setDefenseBehavior(defendWithArmor);
//
//        mysticAdventurer.setAttackBehavior(attackWithMagic);
//        mysticAdventurer.setDefenseBehavior(defendWithShield);
//
//        this.adventurerRepository.save(agileAdventurer);
//        this.adventurerRepository.save(knightAdventurer);
//        this.adventurerRepository.save(mysticAdventurer);
//        this.strategyRepository.addAttackBehavior(attackWithGun);
//        this.strategyRepository.addAttackBehavior(attackWithMagic);
//        this.strategyRepository.addAttackBehavior(attackWithSword);
//        this.strategyRepository.addDefenseBehavior(defendWithBarrier);
//        this.strategyRepository.addDefenseBehavior(defendWithArmor);
//        this.strategyRepository.addDefenseBehavior(defendWithShield);
    }
}