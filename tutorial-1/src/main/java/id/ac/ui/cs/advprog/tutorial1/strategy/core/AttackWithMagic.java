package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    @Override
    public String attack() {
        return "Attacking with Magic";
    }

    @Override
    public String getType() {
        return "Type Magic";
    }
}
