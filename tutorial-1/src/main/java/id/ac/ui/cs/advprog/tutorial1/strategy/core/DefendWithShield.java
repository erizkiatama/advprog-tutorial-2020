package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String defend() {
        return "Defending with Shield";
    }

    @Override
    public String getType() {
        return "Type Shield";
    }}
